# -*- encoding: utf-8 -*-
from django.conf.urls.defaults import patterns, include, url
from django_markdown import flatpages
from django.contrib import admin
from django.contrib.auth.views import login, logout
import settings

admin.autodiscover()
flatpages.register()

urlpatterns = patterns('oasis.views',
    url(r'^$', 'index'),
    url(r'^catalog/$', 'catalog'),
    url(r'^catalog/(?P<id>\d+)/$', 'catalog_info'),
    url(r'^galleries/$', 'galleries'),
    url(r'^gallery/(?P<id>\d+)/$', 'gallery'),
    url(r'^prices/$', 'prices'),
    url(r'^calculator/$', 'calculator'),
    url(r'^calculator/(?P<id>\d+)/$', 'calculator_metric'),
    url(r'^bonuses/$', 'bonuses'),
    url(r'^bonuses/(?P<id>\d+)/$', 'bonuses_info'),    
    url(r'^osform/$', 'osform'),
    url(r'^basket/$', 'basket'),
    url(r'^register/$', 'register'),
    url(r'^search/$', 'search'),
    url(r'^confirm/(?P<confirm_code>\w+)/$', 'confirm'),
    url(r'^confirm_order/$', 'confirm_order'),
    url(r'^clear_basket/$', 'clear_basket'),
    url(r'^sertificate/$', 'sertificate'),
    url(r'^use_sertificate/(?P<id>\d+)/$', 'use_sertificate'),
    url(r'^add_comment/$', 'add_comment'),
    url(r'^comment/$', 'comment'),
    url(r'^advice/(?P<id>\d+)/$', 'advice'),
    url(r'^article/(?P<id>\d+)/$', 'article'),
)
urlpatterns += patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^markdown/', include( 'django_markdown.urls')),
    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^login/$', login, {'template_name':'login.html'}),
    url(r'^logout/$', logout, {'template_name':'index.html'}),
)

if settings.DEBUG:
    urlpatterns += patterns('',
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root':'./media/'}),
)