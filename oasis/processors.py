# -*- coding: utf-8 -*-

from oasis.service.models import Service, Gallery, Comment, Advice, Article
from forms import OSForm

def proc(request):
    nodes = Service.objects.all()
    return{ 'nodes' : nodes,
            'tops' : nodes[:5],
            'osform' : OSForm(),
            'last_projects': Gallery.objects.filter(finish=True),
            'current_projects': Gallery.objects.filter(finish=False),
            'comments': Comment.objects.all(),
            'advices': Advice.objects.all()[:5],
            'articles': Article.objects.all()[:5],
           }