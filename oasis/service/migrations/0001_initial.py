# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Service'
        db.create_table('service_service', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('parent', self.gf('mptt.fields.TreeForeignKey')(blank=True, related_name='children', null=True, to=orm['service.Service'])),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('miniature', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('price', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('metric', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('qmetric', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('lft', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('rght', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('tree_id', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
            ('level', self.gf('django.db.models.fields.PositiveIntegerField')(db_index=True)),
        ))
        db.send_create_signal('service', ['Service'])

        # Adding model 'Gallery'
        db.create_table('service_gallery', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('finish', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('service', ['Gallery'])

        # Adding model 'Photo'
        db.create_table('service_photo', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('parent_gallery', self.gf('django.db.models.fields.related.ForeignKey')(related_name='photo', to=orm['service.Gallery'])),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('photo', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
        ))
        db.send_create_signal('service', ['Photo'])

        # Adding model 'Bonus'
        db.create_table('service_bonus', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('size', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('metric', self.gf('django.db.models.fields.CharField')(default='%', max_length=4)),
        ))
        db.send_create_signal('service', ['Bonus'])

        # Adding M2M table for field service on 'Bonus'
        db.create_table('service_bonus_service', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('bonus', models.ForeignKey(orm['service.bonus'], null=False)),
            ('service', models.ForeignKey(orm['service.service'], null=False))
        ))
        db.create_unique('service_bonus_service', ['bonus_id', 'service_id'])

        # Adding model 'UserProfile'
        db.create_table('service_userprofile', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True)),
            ('confirm_code', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal('service', ['UserProfile'])

        # Adding model 'Sertificate'
        db.create_table('service_sertificate', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('sertificate', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='sertificates', to=orm['auth.User'])),
            ('size', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('metric', self.gf('django.db.models.fields.CharField')(default='%', max_length=4)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('service', ['Sertificate'])

        # Adding model 'Order'
        db.create_table('service_order', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('order', self.gf('django.db.models.fields.related.ForeignKey')(related_name='orders', to=orm['service.Service'])),
            ('summary', self.gf('django.db.models.fields.IntegerField')(max_length=20)),
            ('quantity', self.gf('django.db.models.fields.IntegerField')(max_length=20)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='orders', to=orm['auth.User'])),
            ('remark', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2012, 7, 6, 0, 0))),
            ('performed', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('service', ['Order'])

        # Adding model 'Comment'
        db.create_table('service_comment', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('body', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('service', ['Comment'])

        # Adding model 'Article'
        db.create_table('service_article', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('body', self.gf('django.db.models.fields.TextField')()),
            ('publication_date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2012, 7, 6, 0, 0))),
        ))
        db.send_create_signal('service', ['Article'])

        # Adding model 'Advice'
        db.create_table('service_advice', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('body', self.gf('django.db.models.fields.TextField')()),
            ('publication_date', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2012, 7, 6, 0, 0))),
        ))
        db.send_create_signal('service', ['Advice'])


    def backwards(self, orm):
        # Deleting model 'Service'
        db.delete_table('service_service')

        # Deleting model 'Gallery'
        db.delete_table('service_gallery')

        # Deleting model 'Photo'
        db.delete_table('service_photo')

        # Deleting model 'Bonus'
        db.delete_table('service_bonus')

        # Removing M2M table for field service on 'Bonus'
        db.delete_table('service_bonus_service')

        # Deleting model 'UserProfile'
        db.delete_table('service_userprofile')

        # Deleting model 'Sertificate'
        db.delete_table('service_sertificate')

        # Deleting model 'Order'
        db.delete_table('service_order')

        # Deleting model 'Comment'
        db.delete_table('service_comment')

        # Deleting model 'Article'
        db.delete_table('service_article')

        # Deleting model 'Advice'
        db.delete_table('service_advice')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'service.advice': {
            'Meta': {'object_name': 'Advice'},
            'body': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publication_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2012, 7, 6, 0, 0)'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'service.article': {
            'Meta': {'object_name': 'Article'},
            'body': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publication_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2012, 7, 6, 0, 0)'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'service.bonus': {
            'Meta': {'object_name': 'Bonus'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metric': ('django.db.models.fields.CharField', [], {'default': "'%'", 'max_length': '4'}),
            'service': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'bonuses'", 'symmetrical': 'False', 'to': "orm['service.Service']"}),
            'size': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        'service.comment': {
            'Meta': {'object_name': 'Comment'},
            'body': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'service.gallery': {
            'Meta': {'object_name': 'Gallery'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'finish': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        'service.order': {
            'Meta': {'object_name': 'Order'},
            'date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2012, 7, 6, 0, 0)'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'orders'", 'to': "orm['service.Service']"}),
            'performed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'quantity': ('django.db.models.fields.IntegerField', [], {'max_length': '20'}),
            'remark': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'summary': ('django.db.models.fields.IntegerField', [], {'max_length': '20'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'orders'", 'to': "orm['auth.User']"})
        },
        'service.photo': {
            'Meta': {'object_name': 'Photo'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'parent_gallery': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'photo'", 'to': "orm['service.Gallery']"}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'})
        },
        'service.sertificate': {
            'Meta': {'object_name': 'Sertificate'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metric': ('django.db.models.fields.CharField', [], {'default': "'%'", 'max_length': '4'}),
            'sertificate': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'size': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'sertificates'", 'to': "orm['auth.User']"})
        },
        'service.service': {
            'Meta': {'ordering': "['tree_id', 'lft']", 'object_name': 'Service'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'metric': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'miniature': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': "orm['service.Service']"}),
            'price': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'qmetric': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'service.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            'confirm_code': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True'})
        }
    }

    complete_apps = ['service']