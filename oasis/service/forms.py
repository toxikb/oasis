# -*- encoding: utf-8 -*-

from django import forms
from models import Service

class Calculator(forms.Form):
    service = forms.ModelChoiceField (queryset=Service.objects.all())
    metric = forms.CharField (widget=forms.TextInput(attrs={'readonly': 'readonly'}), required=False)
    price = forms.CharField( widget=forms.TextInput(attrs={'readonly': 'readonly'}))
    quantity = forms.CharField (required=False)
    summary = forms.CharField(widget=forms.TextInput(attrs={'readonly': 'readonly'}), initial=0)
    remark = forms.CharField(widget=forms.Textarea(attrs={'rows':6, 'cols':70}), required=False)