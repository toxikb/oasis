# -*- encoding: utf-8 -*-
from oasis.service.models import *
from django.contrib import admin
from django_markdown.admin import MarkdownModelAdmin
from mptt.admin import MPTTModelAdmin

class ServiceAdmin(MPTTModelAdmin, MarkdownModelAdmin, admin.ModelAdmin):
    list_display=('title','parent', 'description', 'miniature', 'price', 'metric', )
    mptt_level_indent = 20

class GalleryAdmin(MarkdownModelAdmin, admin.ModelAdmin):
    list_display=('title','description', 'finish', )

class PhotoAdmin(MarkdownModelAdmin, admin.ModelAdmin):
    class Media:
        js = ['js/admin/display_thumbs.js']
    list_display = ['photo', 'get_thumbnail_html', '__unicode__', 'description']
class BonusAdmin(MarkdownModelAdmin, admin.ModelAdmin):
    list_display=('title', 'description', 'size', 'metric', )
    filter_horizontal = ('service',)

class UserProfileAdmin(admin.ModelAdmin):
    list_display=('user', 'confirm_code',)

class SertificateAdmin(admin.ModelAdmin):
    list_display=('sertificate', 'user', 'size', 'metric', 'active')

class OrderAdmin(admin.ModelAdmin):
    list_display=('order', 'quantity', 'summary', 'user', 'date', 'performed',)

class CommentAdmin(admin.ModelAdmin):
    list_display=('body', 'user',)

class AdviceAdmin(MarkdownModelAdmin, admin.ModelAdmin):
    list_display=('title', 'body', 'publication_date', )    

class ArticleAdmin(MarkdownModelAdmin, admin.ModelAdmin):
    list_display=('title', 'body', 'publication_date', )

admin.site.register(Service, ServiceAdmin)
admin.site.register(Gallery, GalleryAdmin)
admin.site.register(Photo, PhotoAdmin)
admin.site.register(Bonus, BonusAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Sertificate, SertificateAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(Advice, AdviceAdmin)
admin.site.register(Article, ArticleAdmin)