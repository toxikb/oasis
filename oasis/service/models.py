# -*- encoding: utf-8 -*-

from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from django.utils.translation import ugettext as _
from django.utils.encoding import force_unicode
from django.contrib.auth.models import User
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from utils import *
import datetime

class Service (MPTTModel):
    title = models.CharField(_(u'Название работы'), max_length=40)  
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', verbose_name=u'Родительская ветка')
    description = models.TextField(_(u'Описание работы'), null=True, blank=True)
    miniature = models.ImageField(_(u'Миниатюра'), upload_to='img', max_length=100, blank=True, null=True)
    price = models.FloatField(_(u'Цена работы'), null=True, blank=True)
    metric = models.CharField(_(u'Единица измерения'), max_length=40)
    def __unicode__(self):
        return force_unicode(self.title)
    class Meta():
        verbose_name=u'Услуга'
        verbose_name_plural=u'Услуги'
        ordering = ['tree_id', 'lft']

class Gallery (models.Model):
    title = models.CharField(_(u'Название галереи'), max_length=40)
    description = models.TextField(_(u'Описание галереи'), null=True, blank=True)
    finish = models.BooleanField(_(u'Проект завершен'))
    def __unicode__(self):
        return force_unicode(self.title)
    class Meta():
        verbose_name=u'Галлерея'
        verbose_name_plural=u'Галлереи'

class Photo (models.Model):
    parent_gallery = models.ForeignKey(Gallery, related_name='photo', verbose_name=u'Галлерея')
    description = models.CharField(_(u'Описание фотографии'), null=True, blank=True, max_length=100)
    photo = models.ImageField(upload_to='img', max_length=100, verbose_name=u'Фотография')
    def __unicode__(self):
        return force_unicode(self.photo)
    class Meta():
        verbose_name=u'Фотография'
        verbose_name_plural=u'Фотографии'
    def get_thumbnail_html(self):
        html = '<a class="image-picker" href="%s" rel="lightbox[%s]"><img src="%s" alt="%s"/></a>'
        return html % (self.photo.url, self.parent_gallery.title, get_thumbnail_url(self.photo.url), self.description)
    get_thumbnail_html.short_description = _(u'Миниатюра')
    get_thumbnail_html.allow_tags = True

def post_save_handler(sender, **kwargs):
    create_thumbnail(kwargs['instance'].photo.path)
post_save.connect(post_save_handler, sender=Photo)

def pre_delete_handler(sender, **kwargs):
    delete_thumbnail(kwargs['instance'].photo.path)
pre_delete.connect(pre_delete_handler, sender=Photo)

class Bonus (models.Model):
    metric_choices = (
        ('%', '%'),
        (u'руб.', u'руб.'),
    )
    title = models.CharField(_(u'Название акции'), max_length=40)
    description = models.TextField(_(u'Описание акции'))
    size = models.IntegerField (_(u'Размер скидки'), default=0)
    metric = models.CharField(_(u'Еденица измерения скидки'), max_length=4, choices=metric_choices, default='%')
    service = models.ManyToManyField(Service, related_name='bonuses', verbose_name=u'Услуги')
    def __unicode__(self):
        return force_unicode(self.title)
    class Meta():
        verbose_name=u'Акция'
        verbose_name_plural=u'Акции'

class UserProfile(models.Model):
    user = models.OneToOneField(User, verbose_name=u'Имя пользователя')
    confirm_code = models.CharField(_(u'Код подтверждения'), max_length=100)
    def __unicode__(self):
        return force_unicode(self.user)
    class Meta():
        verbose_name=u'Информация о пользователях'
        verbose_name_plural=u'Информация о пользователях'
@receiver(post_save, sender=User, dispatch_uid='uid')
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)

class Sertificate (models.Model):
    metric_choices = (
        ('%', '%'),
        (u'руб.', u'руб.'),
    )
    sertificate = models.CharField (_(u'Номер подарочного сертификата'), max_length=100)
    user = models.ForeignKey (User, related_name='sertificates', verbose_name=u'Имя пользователя')
    size = models.IntegerField (_(u'Размер скидки'), default=0)
    metric = models.CharField(_(u'Еденица измерения скидки'), max_length=4, choices=metric_choices, default='%')
    active = models.BooleanField(_(u'Сертификат активен'), default=False)
    def __unicode__(self):
        return force_unicode(self.sertificate)
    class Meta():
        verbose_name=u'Сертификат'
        verbose_name_plural=u'Сертификаты'

class Order (models.Model):
    order = models.ForeignKey (Service, related_name='orders', verbose_name=u'Заказ')
    summary = models.IntegerField (_(u'Сумма заказа'), max_length=20)
    quantity = models.IntegerField (_(u'Количество'), max_length=20)
    user = models.ForeignKey (User, related_name='orders', verbose_name=u'Имя пользователя')
    remark = models.TextField(_(u'Примечание'), blank=True, null=True)
    date = models.DateTimeField (_(u'Время заказа'), default=datetime.datetime.now())
    performed = models.BooleanField (_(u'Заказ выполнен'), default=False)
    def __unicode__(self):
        return force_unicode(self.order)
    class Meta():
        verbose_name=u'Заказ'
        verbose_name_plural=u'Заказы'

class Comment(models.Model):
    user = models.ForeignKey (User, verbose_name=u'Имя пользователя')
    body = models.TextField(_(u'Отзыв'))
    def __unicode__(self):
        return force_unicode(self.body)
    class Meta():
        verbose_name=u'Отзыв'
        verbose_name_plural=u'Отзывы'

class Article (models.Model):
    title = models.CharField(_(u'Название статьи'), max_length=100)
    body = models.TextField(_(u'Тело статьи'))
    publication_date = models.DateTimeField (_(u'Дата публикации'), default=datetime.datetime.now())
    def __unicode__(self):
        return force_unicode(self.title)
    class Meta():
        verbose_name=u'Статья'
        verbose_name_plural=u'Статьи'

class Advice (models.Model):
    title = models.CharField(_(u'Название совета'), max_length=100)
    body = models.TextField(_(u'Тело совета'))
    publication_date = models.DateTimeField (_(u'Дата публикации'), default=datetime.datetime.now())
    def __unicode__(self):
        return force_unicode(self.title)
    class Meta():
        verbose_name=u'Совет'
        verbose_name_plural=u'Советы'