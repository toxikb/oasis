# -*- encoding: utf-8 -*-

from django.shortcuts import render_to_response
from django.template import RequestContext
from service.models import *
from service.forms import Calculator
from django.utils import simplejson
from django.http import HttpResponse, HttpResponseRedirect
from forms import *
import hashlib
from django.contrib.auth.models import User
from django.core.mail import EmailMessage
import settings
import datetime
from django.contrib.auth.decorators import login_required

def index(request):
    return render_to_response ('index.html', context_instance=RequestContext(request))

def catalog(request):
    return render_to_response ('catalog.html', {'nodes':Service.objects.all()}, context_instance=RequestContext(request))

def catalog_info(request, id):
    service = Service.objects.get(id=id)
    def get_child(service):
        for child in service.children.all():
            childs.append(child)
            get_child(child)
        return childs
    childs = list()
    children = get_child(service)
    return render_to_response ('catalog_info.html', {'service':service, 'children':children}, context_instance=RequestContext(request))

def galleries(request):
    return render_to_response ('galleries.html', {'galleries':Gallery.objects.all()}, context_instance=RequestContext(request))

def gallery(request, id):
    gallery = Gallery.objects.get(id=id)
    photo = gallery.photo.all()
    return render_to_response ('gallery.html', {'photo':photo}, context_instance=RequestContext(request))

def prices(request):
    return render_to_response ('prices.html', context_instance=RequestContext(request))

def bonuses(request):
    return render_to_response ('bonuses.html', {'bonuses':Bonus.objects.all()}, context_instance=RequestContext(request))

def bonuses_info(request, id):
    return render_to_response ('bonuses_info.html', {'bonus':Bonus.objects.get(id=id)}, context_instance=RequestContext(request))    

def calculator(request):
    if request.method == 'POST':
        form = Calculator(request.POST)
        if form.is_valid():
            service = Service.objects.get(id=request.POST['service'])
            if not 'services' in request.session:
                request.session["services"] = list()
            price = int(request.POST['summary'])
            request.session["services"].append({'title':service.title, 'quantity':request.POST['quantity'], 'price':price, 'remark':request.POST['remark']})
            if 'summary' in request.session:
                request.session["summary"] = int(request.session["summary"])+price
            else:
                request.session["summary"] = price
            return render_to_response ('basket.html', context_instance=RequestContext(request))
    calculator = Calculator()
    return render_to_response ('calculator.html', {'calculator':calculator}, context_instance=RequestContext(request))

def calculator_metric(request, id):
    service = Service.objects.get(id=id)
    data = dict()
    data['price'] = service.price or 0
    data['metric'] = service.metric
    data['bonuses'] = list()
    for bonus in service.bonuses.all():
        data['bonuses'].append({'title':bonus.title, 'size':bonus.size, 'metric': bonus.metric, 'id':bonus.id})
    return HttpResponse(simplejson.dumps(data))

@login_required()
def osform(request):
    if request.method == 'POST':
        form = OSForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            subject = cd['title']
            message = cd['body']
            msg=EmailMessage(subject, message, settings.DEFAULT_FROM_EMAIL , ['toxi-kb@ya.ru',])
            msg.content_subtype = "html"
            msg.send()
            return HttpResponse('ok')
        else:
            return render_to_response ('osform.html', {'osform':form}, context_instance=RequestContext(request))
    return render_to_response ('osform.html', context_instance=RequestContext(request))

def basket(request):
    return render_to_response ('basket.html', context_instance=RequestContext(request))

def clear_basket(request):
    try:
        del request.session['services']
        del request.session["summary"]
        del request.session["message"]
    except:
        pass
    return render_to_response ('basket.html', context_instance=RequestContext(request))    

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            if cd['password1'] == cd['password2']:
                confirm_code = hashlib.sha224(cd['email']).hexdigest()
                subject = u"Подтверждение регистрации на сайте oasis.ru"
                message = u"Для подтверждения регистрации перейдите по адресу: http://127.0.0.1:8000/confirm/%s/" % confirm_code
                msg=EmailMessage(subject, message, settings.DEFAULT_FROM_EMAIL , ['toxi-kb@ya.ru',])
                msg.content_subtype = "html"
                msg.send()
                user = User.objects.create_user(username=cd['username'], email=cd['email'])
                user.first_name = cd['firstname']
                user.last_name = cd['lastname']
                user.is_active = False
                user.set_password(cd['password1'])
                user.save()
                UserProfile.objects.filter(user=user).update(confirm_code=confirm_code)
                return HttpResponse(u"На Ваш почтовый ящик выслан код подтверждения")
        else:
            return render_to_response ('register.html', {'form':form}, context_instance=RequestContext(request))
    form = UserCreationForm()
    return render_to_response ('register.html', {'form':form}, context_instance=RequestContext(request))

def search(request):
    query = request.GET['q']
    results = Service.objects.filter(title__icontains=query)
    if len(results) == 0:
        return render_to_response ('search.html', {'message':u'Ничего не найдено'}, context_instance=RequestContext(request))        
    return render_to_response ('search.html', {'results':results}, context_instance=RequestContext(request))

def confirm(request, confirm_code):
    user = UserProfile.objects.get(confirm_code=confirm_code)
    if user:
        User.objects.filter(username=user).update(is_active = True)
        return HttpResponse(u'Спасибо за регистрацию')
    return HttpResponse(u'Неверный код подтверждения')

def confirm_order(request):
    if 'services' in request.session:
        for service in request.session["services"]:
            order = Service.objects.get(title=service['title'])
            summary = service['price']
            quantity = service['quantity']
            remark = service['remark']
            user = request.user                
            Order.objects.create(order=order, summary=summary, quantity=quantity, user=user, remark=remark)
            try:
                del request.session['services']
                del request.session["summary"]
                del request.session["message"]
            except:
                pass
        return render_to_response ('order_response.html', {'message':u'Ваш заказ успешно добавлен'}, context_instance=RequestContext(request))
    return render_to_response ('order_response.html', {'message':u'В корзине нет товаров!'}, context_instance=RequestContext(request))

def sertificate(request):
    return render_to_response ('sertificate.html', {'sertificates':request.user.sertificates.filter(active=True)}, context_instance=RequestContext(request))

def use_sertificate(request, id):
    if 'summary' in request.session:
        sertificate = Sertificate.objects.get(id=id)
        if sertificate.active:
            summary = request.session["summary"]
            if sertificate.metric == '%':
                summary = summary - summary * sertificate.size / 100
            elif sertificate.metric == u'руб':
                summary = summary - sertificate.size
            request.session["summary"] = summary
            sertificate.active = False
            sertificate.save()
            request.session['message'] = u'Цена указана с учетом применения сертификатов'
            return render_to_response('basket.html', context_instance=RequestContext(request))
    else:
        error = u'В корзине нет товаров'
        return render_to_response('basket.html', {'error':error}, context_instance=RequestContext(request))

def add_comment(request):
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            Comment.objects.create(user = request.user, body=cd['body'])
            return HttpResponse (u'<p>Спасибо за ваш отзыв!</p>')
        return render_to_response ('comment.html', {'osform':form}, context_instance=RequestContext(request))
    return HttpResponseRedirect('/')

def comment(request):
    if request.user.is_authenticated():
        return render_to_response('comment.html', {'form': CommentForm()}, context_instance=RequestContext(request))
    return HttpResponse(u'<p class="response help">Для добавления комментария необходимо авторизоваться</p>')

def advice(request, id):
    advice = Advice.objects.get(id=id)
    return render_to_response('advice.html', {'advice':advice}, context_instance=RequestContext(request))

def article(request, id):
    article = Article.objects.get(id=id)
    return render_to_response('article.html', {'article':article}, context_instance=RequestContext(request))