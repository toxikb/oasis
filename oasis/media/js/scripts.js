$(function(){
    var myModal = $('.mymodal')
    var backDrop = $('.modal-backdrop')
    var photo = $('.photo')
    $('#osform').on('click', function() {
        $.get('/osform/', function(data){
            backDrop.show()
            myModal.empty().show().append(data)
        });
        return false;
    });
    $('#submit-osform').live('click', function(){
        form = $('form[action="/osform/"]').serializeArray();
        $.post('/osform/', form, function(data){
            if (data =='ok'){
                document.location.href = '/'
            }
            else {
                myModal.empty().append(data)
            }
        });
    });
    $('#comment').on('click', function(){
        $.get('/comment/', function(data){
            $('.response, form[action="/add_comment/"]').remove()
            $('#comment').after(data)
        })
        return false;
    })
    $('#add_comment').live('click', function(){
        form = $('form[action="/add_comment/"]').serializeArray();
        $.post('/add_comment/', form, function(data){
            $('.response, form[action="/add_comment/"]').remove()
            $('#comment').after(data)
        });
    })

    $('#nav a[href!="/login/"][href!="/logout/"], #top a, #all-service a, #projects a, #extra a, #main-menu a, a[href="/confirm_order/"]').live('click', function(){
        $.get(this.href, function(data){
            $('#main-menu').empty().append(data)
        })
        return false;
    })

    backDrop.on('click', function(){
        backDrop.hide()
        myModal.hide()
        photo.hide()
    });
    $('.parent').live('click', function(){
        $(this).find('i').toggleClass('icon-down-open').toggleClass('icon-right-open')
        $(this).next('.children').slideToggle(400)
    });
    $('#id_service').live('change', function(){
        $.getJSON('/calculator/'+$(this).val()+'/', function(data){
            $('.bonus').empty()
            $('#id_metric').attr('value', data['metric'])
            $('#id_price').attr('value', data['price'])
            $.each(data['bonuses'], function(k, v){
                $('.bonus').append('<a href="/bonus/'+v.id+'/">'+v.title+'</a><input readonly class="bonus_size" \
                type="text" value='+v.size+'><input readonly class="bonus_metric" type="text" value='+v.metric+'></br>')
            })
        });
        return false;
    });
    $('#calculate').live('click', function(){
        var price = $('#id_price').val()
        var quantity = $('#id_quantity').val()
        var summary = price*quantity
        $('.bonus_size').each(function(){
            $this = $(this)
            var size = $this.val()
            var metric = $this.next('.bonus_metric').val()
            if ( metric == '%'){
                summary = summary - summary * size / 100;
            }
            if ( metric == 'руб'){
                summary = summary - size;
            }
        });
        $('#id_summary').attr('value', summary)
    });
});