# -*- encoding: utf-8 -*-

from django import forms

class OSForm(forms.Form):
    title = forms.CharField()
    body = forms.CharField(widget=forms.Textarea(attrs={'rows':6, 'cols':70}))

class UserCreationForm(forms.Form):
    username = forms.CharField()
    firstname = forms.CharField()
    lastname = forms.CharField()
    password1 = forms.CharField(widget=forms.PasswordInput)
    password2 = forms.CharField(widget=forms.PasswordInput)
    email = forms.EmailField()
    def clean_password2(self):
        pas1 = self.cleaned_data['password1']
        pas2 = self.cleaned_data['password2']
        if not pas1 == pas2:
            raise forms.ValidationError(u'Пароли не совпадают')
        return pas2

class CommentForm(forms.Form):
    body = forms.CharField(widget=forms.Textarea(attrs={'rows':6, 'cols':70}))