#!/home/vadim/.virtualenvs/oasis/bin/python
# -*- coding: utf-8 -*-

activate_this = '/home/vadim/.virtualenvs/oasis/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))
import os
import sys
sys.path.insert(0, os.path.dirname(__file__))
sys.path.insert(0, '/home/vadim/.virtualenvs/oasis/lib/python2.7/site-packages/django')
sys.path.insert(0, '/home/vadim/workspace/oasis/')
sys.path.insert(0, '/home/vadim/workspace/oasis/oasis/')
os.environ['DJANGO_SETTINGS_MODULE'] = 'oasis.settings'
import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
