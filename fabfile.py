from fabric.api import *
env.hosts = ['root@62.76.44.102']
def deploy():
    try:
        local ('cd /home/vadim/workspace/oasis/oasis/ && rm *.pyc')
        local ('cd /home/vadim/workspace/oasis/oasis/service/ && rm *.pyc')
    except:
        pass
    local("git add * && git commit -am '%s'" % prompt("commit_name:"))
    local("git push unfuddle master")
    with cd("/home/django-projects/oasis/"):
        run("git pull")
    with cd('/home/django-projects/oasis/oasis/'):
        run('python manage.py migrate')