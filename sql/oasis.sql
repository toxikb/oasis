-- MySQL dump 10.13  Distrib 5.1.62, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: oazis
-- ------------------------------------------------------
-- Server version	5.1.62-0ubuntu0.11.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_tools_dashboard_preferences`
--

DROP TABLE IF EXISTS `admin_tools_dashboard_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_tools_dashboard_preferences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `data` longtext NOT NULL,
  `dashboard_id` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_tools_dashboard_preferences_fbfc09f1` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_tools_dashboard_preferences`
--

LOCK TABLES `admin_tools_dashboard_preferences` WRITE;
/*!40000 ALTER TABLE `admin_tools_dashboard_preferences` DISABLE KEYS */;
INSERT INTO `admin_tools_dashboard_preferences` VALUES (1,1,'{\"positions\":{},\"columns\":{},\"disabled\":{\"module_5\":true,\"module_6\":true},\"collapsed\":{\"module_3\":false,\"module_2\":false,\"module_4\":false}}','dashboard'),(2,1,'{\"positions\":[\"module_1\",\"module_2\"],\"columns\":[1,1],\"disabled\":{},\"collapsed\":{}}','service-dashboard'),(3,1,'{}','sites-dashboard'),(4,1,'{}','auth-dashboard'),(5,1,'{}','flatpages-dashboard');
/*!40000 ALTER TABLE `admin_tools_dashboard_preferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_tools_menu_bookmark`
--

DROP TABLE IF EXISTS `admin_tools_menu_bookmark`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_tools_menu_bookmark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_tools_menu_bookmark_fbfc09f1` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_tools_menu_bookmark`
--

LOCK TABLES `admin_tools_menu_bookmark` WRITE;
/*!40000 ALTER TABLE `admin_tools_menu_bookmark` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_tools_menu_bookmark` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_bda51c3c` (`group_id`),
  KEY `auth_group_permissions_1e014c8f` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_message`
--

DROP TABLE IF EXISTS `auth_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_message_fbfc09f1` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_message`
--

LOCK TABLES `auth_message` WRITE;
/*!40000 ALTER TABLE `auth_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_e4470c6e` (`content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add permission',1,'add_permission'),(2,'Can change permission',1,'change_permission'),(3,'Can delete permission',1,'delete_permission'),(4,'Can add group',2,'add_group'),(5,'Can change group',2,'change_group'),(6,'Can delete group',2,'delete_group'),(7,'Can add user',3,'add_user'),(8,'Can change user',3,'change_user'),(9,'Can delete user',3,'delete_user'),(10,'Can add message',4,'add_message'),(11,'Can change message',4,'change_message'),(12,'Can delete message',4,'delete_message'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add site',7,'add_site'),(20,'Can change site',7,'change_site'),(21,'Can delete site',7,'delete_site'),(22,'Can add log entry',8,'add_logentry'),(23,'Can change log entry',8,'change_logentry'),(24,'Can delete log entry',8,'delete_logentry'),(25,'Can add service',9,'add_service'),(26,'Can change service',9,'change_service'),(27,'Can delete service',9,'delete_service'),(28,'Can add Галлерея',10,'add_gallery'),(29,'Can change Галлерея',10,'change_gallery'),(30,'Can delete Галлерея',10,'delete_gallery'),(31,'Can add Фотография',11,'add_photo'),(32,'Can change Фотография',11,'change_photo'),(33,'Can delete Фотография',11,'delete_photo'),(72,'Can delete Статья',24,'delete_article'),(71,'Can change Статья',24,'change_article'),(70,'Can add Статья',24,'add_article'),(37,'Can add bookmark',13,'add_bookmark'),(38,'Can change bookmark',13,'change_bookmark'),(39,'Can delete bookmark',13,'delete_bookmark'),(40,'Can add dashboard preferences',14,'add_dashboardpreferences'),(41,'Can change dashboard preferences',14,'change_dashboardpreferences'),(42,'Can delete dashboard preferences',14,'delete_dashboardpreferences'),(43,'Can add page',15,'add_page'),(44,'Can change page',15,'change_page'),(45,'Can delete page',15,'delete_page'),(46,'Can add flat page',16,'add_flatpage'),(47,'Can change flat page',16,'change_flatpage'),(48,'Can delete flat page',16,'delete_flatpage'),(68,'Can change Акция',23,'change_bonus'),(67,'Can add Акция',23,'add_bonus'),(59,'Can change Сертификат',20,'change_sertificate'),(58,'Can add Сертификат',20,'add_sertificate'),(55,'Can add user profile',19,'add_userprofile'),(56,'Can change user profile',19,'change_userprofile'),(57,'Can delete user profile',19,'delete_userprofile'),(60,'Can delete Сертификат',20,'delete_sertificate'),(61,'Can add Заказ',21,'add_order'),(62,'Can change Заказ',21,'change_order'),(63,'Can delete Заказ',21,'delete_order'),(64,'Can add Отзыв',22,'add_comment'),(65,'Can change Отзыв',22,'change_comment'),(66,'Can delete Отзыв',22,'delete_comment'),(69,'Can delete Акция',23,'delete_bonus'),(73,'Can add Совет',25,'add_advice'),(74,'Can change Совет',25,'change_advice'),(75,'Can delete Совет',25,'delete_advice');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `password` varchar(128) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `last_login` datetime NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'admin','','','admin@oazis.ru','sha1$a6494$516130bffe23b79dbe1f09840f82565b212e7e81',1,1,1,'2012-07-05 12:30:41','2012-06-19 00:41:52'),(11,'qqq','123','qwe','toxi-kb@ya.ru','sha1$73c00$868425e801590d605ec953755b9225f21ae99ee4',0,1,0,'2012-07-03 11:37:31','2012-06-22 15:52:41');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_fbfc09f1` (`user_id`),
  KEY `auth_user_groups_bda51c3c` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_fbfc09f1` (`user_id`),
  KEY `auth_user_user_permissions_1e014c8f` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_fbfc09f1` (`user_id`),
  KEY `django_admin_log_e4470c6e` (`content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=253 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2012-06-19 01:23:02',1,9,'2','Монтаж стены',1,''),(2,'2012-06-19 01:23:19',1,9,'1','Монтаж стены',3,''),(3,'2012-06-19 11:12:22',1,9,'2','Монтаж стены',3,''),(4,'2012-06-19 11:19:10',1,9,'3','Ремонт квартир',1,''),(5,'2012-06-19 11:20:36',1,9,'4','Отделочные работы',1,''),(6,'2012-06-19 12:43:44',1,9,'5','Сантехника',1,''),(7,'2012-06-19 12:44:44',1,9,'6','Отделка офисов',1,''),(8,'2012-06-19 12:45:06',1,9,'7','Малярные работы',1,''),(9,'2012-06-19 12:45:13',1,9,'8','Пол',1,''),(10,'2012-06-19 12:45:25',1,9,'9','Укладка плитки',1,''),(11,'2012-06-19 12:48:14',1,9,'10','Штукатурка',1,''),(12,'2012-06-19 13:34:30',1,9,'10','Штукатурка',2,'Изменен miniature.'),(13,'2012-06-19 13:40:06',1,9,'10','Штукатурка',2,'Изменен miniature.'),(14,'2012-06-19 13:42:13',1,9,'10','Штукатурка',2,'Изменен miniature.'),(15,'2012-06-19 14:23:29',1,9,'10','Штукатурка',2,'Изменен miniature.'),(16,'2012-06-19 15:26:50',1,9,'10','Штукатурка',2,'Изменен miniature.'),(17,'2012-06-19 15:34:58',1,9,'10','Штукатурка',2,'Изменен price.'),(18,'2012-06-19 15:46:31',1,16,'1','/about/ -- О компании',1,''),(19,'2012-06-19 15:49:55',1,16,'1','/about/ -- О компании',2,'Изменен template_name.'),(20,'2012-06-19 15:51:28',1,16,'2','/contacts/ -- Все контакты',1,''),(21,'2012-06-19 15:52:08',1,16,'3','/map/ -- Схема проезда',1,''),(22,'2012-06-19 15:54:07',1,16,'3','/map/ -- Схема проезда',2,'Изменен content.'),(23,'2012-06-19 15:54:41',1,16,'3','/map/ -- Схема проезда',2,'Изменен content.'),(24,'2012-06-19 15:56:20',1,16,'3','/map/ -- Схема проезда',2,'Изменен template_name.'),(25,'2012-06-19 15:56:41',1,16,'3','/map/ -- Схема проезда',2,'Изменен template_name.'),(26,'2012-06-19 15:56:49',1,16,'3','/map/ -- Схема проезда',2,'Ни одно поле не изменено.'),(27,'2012-06-19 15:57:26',1,16,'3','/map/ -- Схема проезда',2,'Изменен content.'),(28,'2012-06-19 16:04:25',1,16,'3','/map/ -- Схема проезда',2,'Изменен content.'),(29,'2012-06-19 16:04:44',1,16,'3','/map/ -- Схема проезда',2,'Изменен content.'),(30,'2012-06-19 16:08:34',1,16,'1','/about/ -- О компании',2,'Изменен content.'),(31,'2012-06-19 16:08:44',1,16,'2','/contacts/ -- Все контакты',2,'Изменен content.'),(32,'2012-06-20 09:56:41',1,16,'3','/map/ -- Схема проезда',2,'Изменен content.'),(87,'2012-06-26 14:13:04',1,9,'1','Ремонт квартир',1,''),(35,'2012-06-20 13:22:22',1,9,'9','Укладка плитки',2,'Изменен description.'),(36,'2012-06-22 10:37:32',1,16,'1','/about/ -- О компании',2,'Изменен content.'),(37,'2012-06-22 15:09:41',1,19,'7','UserProfile object',3,''),(38,'2012-06-22 15:09:41',1,19,'6','UserProfile object',3,''),(39,'2012-06-22 15:09:41',1,19,'5','UserProfile object',3,''),(40,'2012-06-22 15:09:41',1,19,'4','UserProfile object',3,''),(41,'2012-06-22 15:09:41',1,19,'3','UserProfile object',3,''),(42,'2012-06-22 15:09:41',1,19,'2','UserProfile object',3,''),(43,'2012-06-22 15:09:41',1,19,'1','UserProfile object',3,''),(44,'2012-06-22 15:10:12',1,3,'6','q',3,''),(45,'2012-06-22 15:10:12',1,3,'7','q111',3,''),(46,'2012-06-22 15:10:12',1,3,'8','q11122',3,''),(47,'2012-06-22 15:10:12',1,3,'9','q1112212312',3,''),(48,'2012-06-22 15:10:12',1,3,'2','toxi-kb',3,''),(49,'2012-06-22 15:10:12',1,3,'3','toxikb',3,''),(50,'2012-06-22 15:10:12',1,3,'4','toxikb111',3,''),(51,'2012-06-22 15:10:12',1,3,'5','toxikb11122',3,''),(52,'2012-06-22 15:53:19',1,3,'10','q',3,''),(53,'2012-06-25 09:39:31',1,9,'10','Штукатурка',2,'Изменен miniature.'),(54,'2012-06-25 09:40:46',1,9,'10','Штукатурка',2,'Изменен miniature.'),(55,'2012-06-25 09:40:54',1,9,'10','Штукатурка',2,'Изменен miniature.'),(56,'2012-06-25 13:31:08',1,20,'1','123456',1,''),(57,'2012-06-25 13:37:45',1,20,'1','654321',1,''),(58,'2012-06-25 13:37:52',1,20,'2','123456',1,''),(59,'2012-06-25 13:45:25',1,21,'1','Штукатурка',1,''),(60,'2012-06-25 14:25:47',1,10,'1','Галлерея 1',1,''),(61,'2012-06-25 14:25:53',1,10,'2','Галлерея 2',1,''),(62,'2012-06-25 14:25:58',1,10,'3','Галлерея 3',1,''),(63,'2012-06-25 14:26:02',1,10,'4','Галлерея 4',1,''),(64,'2012-06-25 14:34:47',1,10,'2','Галлерея 2',2,'Изменен finish.'),(65,'2012-06-25 14:34:51',1,10,'1','Галлерея 1',2,'Изменен finish.'),(66,'2012-06-25 14:46:13',1,11,'2','img/1_1.jpeg',1,''),(67,'2012-06-25 14:46:30',1,11,'3','img/2.jpeg',1,''),(68,'2012-06-25 14:46:39',1,11,'4','img/3.jpeg',1,''),(69,'2012-06-25 14:46:43',1,11,'5','img/4.jpeg',1,''),(70,'2012-06-25 14:46:48',1,11,'6','img/5.jpeg',1,''),(71,'2012-06-25 14:46:55',1,11,'7','img/6.jpeg',1,''),(72,'2012-06-25 14:46:59',1,11,'8','img/7.jpeg',1,''),(73,'2012-06-25 14:47:04',1,11,'9','img/8.jpeg',1,''),(74,'2012-06-25 14:49:48',1,11,'2','img/1_1.jpeg',3,''),(75,'2012-06-25 15:28:06',1,11,'9','img/8.jpeg',2,'Ни одно поле не изменено.'),(76,'2012-06-25 15:28:26',1,11,'8','img/7.jpeg',2,'Ни одно поле не изменено.'),(77,'2012-06-25 15:30:01',1,11,'7','img/6.jpeg',2,'Ни одно поле не изменено.'),(78,'2012-06-25 15:30:04',1,11,'6','img/5.jpeg',2,'Ни одно поле не изменено.'),(79,'2012-06-25 15:30:09',1,11,'5','img/4.jpeg',2,'Ни одно поле не изменено.'),(80,'2012-06-25 15:30:12',1,11,'4','img/3.jpeg',2,'Ни одно поле не изменено.'),(81,'2012-06-25 15:30:16',1,11,'3','img/2.jpeg',2,'Ни одно поле не изменено.'),(82,'2012-06-25 15:30:21',1,11,'1','img/1.jpeg',2,'Ни одно поле не изменено.'),(83,'2012-06-25 15:30:29',1,11,'1','img/1.jpeg',3,''),(84,'2012-06-25 15:34:57',1,11,'4','img/3.jpeg',2,'Ни одно поле не изменено.'),(85,'2012-06-25 15:53:21',1,22,'1','Заказывал пол! Отличная работа!',1,''),(86,'2012-06-25 15:54:12',1,22,'2','Имела счастье работать с этой компанией, остались только положительные эмоции, спасибо! )',1,''),(88,'2012-06-26 14:13:17',1,9,'2','Отделка офисов',1,''),(89,'2012-06-26 14:14:20',1,9,'3','Отделочные работы',1,''),(90,'2012-06-26 14:14:34',1,9,'4','Сантехника',1,''),(91,'2012-06-26 14:14:48',1,9,'5','Малярные работы',1,''),(92,'2012-06-26 14:15:02',1,9,'6','Укладка плитки',1,''),(93,'2012-06-26 14:15:18',1,9,'7','Поклейка обоев',1,''),(94,'2012-06-26 14:15:52',1,9,'8','Пол',1,''),(95,'2012-06-26 14:16:14',1,9,'6','Укладка плитки',2,'Изменен price.'),(96,'2012-06-26 14:16:22',1,9,'4','Сантехника',2,'Изменен price.'),(97,'2012-06-26 14:16:31',1,9,'5','Малярные работы',2,'Изменен price.'),(98,'2012-06-26 14:16:36',1,9,'6','Укладка плитки',2,'Ни одно поле не изменено.'),(99,'2012-06-26 14:16:44',1,9,'3','Отделочные работы',2,'Изменен price.'),(100,'2012-06-26 14:16:51',1,9,'7','Поклейка обоев',2,'Изменен price.'),(101,'2012-06-26 14:17:03',1,9,'8','Пол',2,'Изменен price.'),(109,'2012-06-27 10:06:15',1,23,'3','Скидка на поклейку обоев!',2,'Изменен metric.'),(108,'2012-06-27 10:05:44',1,23,'3','Скидка на поклейку обоев!',1,''),(107,'2012-06-27 10:02:06',1,23,'2','Скидка на сантехнику!',1,''),(106,'2012-06-27 10:01:12',1,23,'1','Скидка на все услуги в июне!',1,''),(110,'2012-06-27 11:37:56',1,20,'2','123456',1,''),(111,'2012-06-27 11:38:01',1,20,'1','123456',3,''),(112,'2012-06-27 11:38:19',1,20,'3','654321',1,''),(113,'2012-06-27 12:37:58',1,20,'3','654321',2,'Изменен active.'),(114,'2012-06-27 12:38:09',1,20,'2','123456',2,'Изменен metric и active.'),(115,'2012-06-27 12:38:56',1,20,'2','123456',2,'Изменен metric.'),(116,'2012-06-27 12:48:19',1,20,'3','654321',2,'Изменен active.'),(117,'2012-06-27 12:48:22',1,20,'2','123456',2,'Изменен active.'),(118,'2012-06-27 12:53:15',1,20,'3','654321',2,'Изменен active.'),(119,'2012-06-27 12:53:19',1,20,'2','123456',2,'Изменен active.'),(120,'2012-06-27 12:55:52',1,20,'3','654321',2,'Изменен active.'),(121,'2012-06-27 12:55:56',1,20,'2','123456',2,'Изменен active.'),(122,'2012-06-27 12:56:44',1,20,'2','123456',2,'Изменен active.'),(123,'2012-06-27 13:28:26',1,20,'3','654321',2,'Изменен active.'),(124,'2012-06-28 15:39:45',1,9,'9','Установка ванн',1,''),(125,'2012-06-28 15:40:05',1,9,'10','Установка унитазов',1,''),(126,'2012-06-28 15:40:19',1,9,'4','Сантехника',2,'Изменен metric.'),(127,'2012-06-28 15:40:54',1,9,'11','Прокладка труб',1,''),(128,'2012-06-28 15:41:02',1,9,'11','Прокладка труб',2,'Изменен metric.'),(129,'2012-06-28 15:41:08',1,9,'11','Прокладка труб',2,'Изменен metric.'),(130,'2012-06-30 12:15:34',1,16,'1','/about/ -- О компании',2,'Изменен content.'),(131,'2012-06-30 12:34:47',1,25,'1','Покраска',1,''),(132,'2012-06-30 12:35:16',1,24,'1','Как правильно ложить пол',1,''),(133,'2012-06-30 13:27:57',1,7,'1','oasis.ru',2,'Изменен domain и name.'),(134,'2012-06-30 13:35:00',1,20,'3','654321',2,'Изменен active.'),(135,'2012-06-30 13:35:04',1,20,'2','123456',2,'Изменен active.'),(136,'2012-06-30 13:36:00',1,21,'2','Пол',3,''),(137,'2012-06-30 13:36:00',1,21,'1','Отделочные работы',3,''),(138,'2012-06-30 13:36:06',1,21,'4','Малярные работы',3,''),(139,'2012-06-30 13:36:06',1,21,'3','Сантехника',3,''),(140,'2012-06-30 13:42:17',1,16,'4','/rules/ -- Правила использования калькулятора',1,''),(141,'2012-06-30 13:47:33',1,16,'4','/rules/ -- Правила использования калькулятора',2,'Изменен content.'),(142,'2012-07-03 09:29:43',1,16,'4','/rules/ -- Правила использования калькулятора',2,'Изменен content.'),(143,'2012-07-03 09:37:32',1,20,'3','654321',2,'Изменен active.'),(144,'2012-07-03 09:37:36',1,20,'2','123456',2,'Изменен active.'),(145,'2012-07-03 11:35:38',1,21,'7','Сантехника',3,''),(146,'2012-07-03 11:35:38',1,21,'6','Пол',3,''),(147,'2012-07-03 11:35:38',1,21,'5','Сантехника',3,''),(148,'2012-07-03 11:40:12',1,21,'9','Сантехника',2,'Ни одно поле не изменено.'),(149,'2012-07-03 12:26:06',1,22,'6','qq',3,''),(150,'2012-07-03 12:26:06',1,22,'5','Еще отзыв',3,''),(151,'2012-07-03 12:26:06',1,22,'4','Отзыв',3,''),(152,'2012-07-03 12:44:24',1,11,'4','img/3_5.jpeg',2,'Изменен photo.'),(153,'2012-07-03 12:47:47',1,11,'4','img/3_8.jpeg',2,'Изменен photo.'),(154,'2012-07-03 12:47:54',1,11,'4','img/3_9.jpeg',2,'Изменен photo.'),(155,'2012-07-03 12:49:43',1,11,'4','img/3.jpeg',2,'Изменен photo.'),(156,'2012-07-03 12:50:26',1,11,'7','img/6.jpeg',2,'Изменен photo.'),(157,'2012-07-03 12:50:58',1,11,'9','img/8.jpeg',3,''),(158,'2012-07-03 12:50:59',1,11,'8','img/7.jpeg',3,''),(159,'2012-07-03 12:50:59',1,11,'7','img/6.jpeg',3,''),(160,'2012-07-03 12:50:59',1,11,'6','img/5.jpeg',3,''),(161,'2012-07-03 12:50:59',1,11,'5','img/4.jpeg',3,''),(162,'2012-07-03 12:50:59',1,11,'4','img/3.jpeg',3,''),(163,'2012-07-03 12:50:59',1,11,'3','img/2.jpeg',3,''),(164,'2012-07-03 12:51:05',1,11,'10','img/1.jpeg',1,''),(165,'2012-07-03 12:52:00',1,11,'11','img/2.jpeg',1,''),(166,'2012-07-03 12:55:04',1,11,'14','img/4_1.jpeg',1,''),(167,'2012-07-03 12:55:33',1,11,'15','img/7.jpeg',1,''),(168,'2012-07-03 12:55:51',1,11,'15','img/7.jpeg',3,''),(169,'2012-07-03 12:55:51',1,11,'14','img/4_1.jpeg',3,''),(170,'2012-07-03 12:55:51',1,11,'13','img/4.jpeg',3,''),(171,'2012-07-03 12:55:51',1,11,'12','img/3_1.jpeg',3,''),(172,'2012-07-03 12:55:51',1,11,'11','img/2.jpeg',3,''),(173,'2012-07-03 12:55:51',1,11,'10','img/1.jpeg',3,''),(174,'2012-07-03 12:55:59',1,11,'16','img/1_1.jpeg',1,''),(175,'2012-07-03 12:56:08',1,11,'17','img/3_2.jpeg',1,''),(176,'2012-07-03 12:56:19',1,11,'18','img/4_2.jpeg',1,''),(177,'2012-07-03 12:56:27',1,11,'19','img/5.jpeg',1,''),(178,'2012-07-03 12:56:37',1,11,'20','img/6_1.jpeg',1,''),(179,'2012-07-03 12:56:48',1,11,'21','img/7_1.jpeg',1,''),(180,'2012-07-03 12:56:59',1,11,'22','img/8.jpeg',1,''),(181,'2012-07-03 13:09:47',1,11,'23','img/9.JPG',1,''),(182,'2012-07-03 14:35:08',1,11,'23','img/9_2.JPG',3,''),(183,'2012-07-03 14:36:51',1,11,'22','img/1_2.jpeg',2,'Изменен photo.'),(184,'2012-07-03 14:36:58',1,11,'21','img/2_1.jpeg',2,'Изменен photo.'),(185,'2012-07-03 14:37:08',1,11,'20','img/3_3.jpeg',2,'Изменен photo.'),(186,'2012-07-03 14:37:14',1,11,'19','img/4_3.jpeg',2,'Изменен photo.'),(187,'2012-07-03 14:37:20',1,11,'18','img/5_1.jpeg',2,'Изменен photo.'),(188,'2012-07-03 14:37:28',1,11,'17','img/7_2.jpeg',2,'Изменен photo.'),(189,'2012-07-03 14:37:35',1,11,'16','img/6_2.jpeg',2,'Изменен photo.'),(190,'2012-07-03 14:37:46',1,11,'24','img/8_1.jpeg',1,''),(191,'2012-07-03 14:37:54',1,11,'25','img/9_3.JPG',1,''),(192,'2012-07-03 14:47:56',1,11,'16','img/6_2.jpeg',2,'Изменен parent_gallery.'),(193,'2012-07-03 14:48:00',1,11,'24','img/8_1.jpeg',2,'Изменен parent_gallery.'),(194,'2012-07-03 15:38:03',1,11,'25','img/9.JPG',2,'Изменен photo.'),(195,'2012-07-03 15:38:09',1,11,'24','img/8.jpeg',2,'Изменен photo.'),(196,'2012-07-03 15:38:15',1,11,'22','img/1.jpeg',2,'Изменен photo.'),(197,'2012-07-03 15:38:20',1,11,'21','img/2.jpeg',2,'Изменен photo.'),(198,'2012-07-03 15:38:27',1,11,'20','img/3.jpeg',2,'Изменен photo.'),(199,'2012-07-03 15:38:35',1,11,'19','img/4.jpeg',2,'Изменен photo.'),(200,'2012-07-03 15:38:41',1,11,'18','img/5.jpeg',2,'Изменен photo.'),(201,'2012-07-03 15:38:47',1,11,'17','img/7.jpeg',2,'Изменен photo.'),(202,'2012-07-03 15:39:01',1,11,'16','img/8_1.jpeg',2,'Изменен photo.'),(203,'2012-07-04 09:57:04',1,11,'25','img/9.JPG',3,''),(204,'2012-07-04 09:57:04',1,11,'24','img/8.jpeg',3,''),(205,'2012-07-04 09:57:04',1,11,'22','img/1.jpeg',3,''),(206,'2012-07-04 09:57:04',1,11,'21','img/2.jpeg',3,''),(207,'2012-07-04 09:57:04',1,11,'20','img/3.jpeg',3,''),(208,'2012-07-04 09:57:04',1,11,'19','img/4.jpeg',3,''),(209,'2012-07-04 09:57:04',1,11,'18','img/5.jpeg',3,''),(210,'2012-07-04 09:57:04',1,11,'17','img/7.jpeg',3,''),(211,'2012-07-04 09:57:04',1,11,'16','img/8_1.jpeg',3,''),(212,'2012-07-04 09:57:11',1,11,'26','img/1.jpeg',1,''),(213,'2012-07-04 09:57:20',1,11,'27','img/3.jpeg',1,''),(214,'2012-07-04 09:57:28',1,11,'28','img/4.jpeg',1,''),(215,'2012-07-04 09:58:53',1,11,'29','img/5.jpeg',1,''),(216,'2012-07-04 09:59:00',1,11,'30','img/6.jpeg',1,''),(217,'2012-07-04 09:59:10',1,11,'31','img/7.jpeg',1,''),(218,'2012-07-04 10:03:41',1,11,'32','img/8.jpeg',1,''),(219,'2012-07-04 10:03:50',1,11,'33','img/9.JPG',1,''),(220,'2012-07-04 10:11:12',1,11,'34','img/10.jpg',1,''),(221,'2012-07-04 10:12:04',1,11,'35','img/11.jpg',1,''),(222,'2012-07-04 10:12:41',1,11,'35','img/11.jpg',3,''),(223,'2012-07-04 10:12:41',1,11,'34','img/10.jpg',3,''),(224,'2012-07-04 10:12:41',1,11,'33','img/9.JPG',3,''),(225,'2012-07-04 10:12:41',1,11,'32','img/8.jpeg',3,''),(226,'2012-07-04 10:12:41',1,11,'31','img/7.jpeg',3,''),(227,'2012-07-04 10:12:41',1,11,'30','img/6.jpeg',3,''),(228,'2012-07-04 10:12:41',1,11,'29','img/5.jpeg',3,''),(229,'2012-07-04 10:12:41',1,11,'28','img/4.jpeg',3,''),(230,'2012-07-04 10:12:41',1,11,'27','img/3.jpeg',3,''),(231,'2012-07-04 10:12:41',1,11,'26','img/1.jpeg',3,''),(232,'2012-07-04 10:12:48',1,11,'36','img/1_1.jpeg',1,''),(233,'2012-07-04 10:13:50',1,11,'37','img/3_1.jpeg',1,''),(234,'2012-07-04 10:13:58',1,11,'38','img/4_1.jpeg',1,''),(235,'2012-07-04 10:23:18',1,11,'39','img/5_1.jpeg',1,''),(236,'2012-07-04 10:23:27',1,11,'40','img/6_1.jpeg',1,''),(237,'2012-07-04 10:23:37',1,11,'41','img/7_1.jpeg',1,''),(238,'2012-07-04 10:23:49',1,11,'42','img/8_1.jpeg',1,''),(239,'2012-07-04 10:24:01',1,11,'43','img/9_1.JPG',1,''),(240,'2012-07-04 10:24:10',1,11,'44','img/10_1.jpg',1,''),(241,'2012-07-04 10:24:18',1,11,'45','img/11_1.jpg',1,''),(242,'2012-07-04 10:30:00',1,11,'44','img/10_2.jpg',2,'Изменен photo.'),(243,'2012-07-04 10:33:36',1,11,'44','img/10_3.jpg',2,'Изменен photo.'),(244,'2012-07-04 10:33:54',1,11,'45','img/11_2.jpg',2,'Изменен photo.'),(245,'2012-07-04 10:35:15',1,11,'43','img/9_2.JPG',2,'Изменен photo.'),(246,'2012-07-04 10:35:22',1,11,'42','img/8_2.jpeg',2,'Изменен photo.'),(247,'2012-07-04 10:35:28',1,11,'41','img/6_2.jpeg',2,'Изменен photo.'),(248,'2012-07-04 10:35:34',1,11,'40','img/5_2.jpeg',2,'Изменен photo.'),(249,'2012-07-04 10:35:44',1,11,'39','img/4_2.jpeg',2,'Изменен photo.'),(250,'2012-07-04 10:35:53',1,11,'38','img/7_2.jpeg',2,'Изменен photo.'),(251,'2012-07-04 10:35:59',1,11,'37','img/3_2.jpeg',2,'Изменен photo.'),(252,'2012-07-04 10:36:04',1,11,'36','img/1_2.jpeg',2,'Изменен photo.');
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'permission','auth','permission'),(2,'group','auth','group'),(3,'user','auth','user'),(4,'message','auth','message'),(5,'content type','contenttypes','contenttype'),(6,'session','sessions','session'),(7,'site','sites','site'),(8,'log entry','admin','logentry'),(9,'service','service','service'),(10,'Галлерея','service','gallery'),(11,'Фотография','service','photo'),(24,'Статья','service','article'),(13,'bookmark','menu','bookmark'),(14,'dashboard preferences','dashboard','dashboardpreferences'),(15,'page','page','page'),(16,'flat page','flatpages','flatpage'),(23,'Акция','service','bonus'),(20,'Сертификат','service','sertificate'),(19,'user profile','service','userprofile'),(21,'Заказ','service','order'),(22,'Отзыв','service','comment'),(25,'Совет','service','advice');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_flatpage`
--

DROP TABLE IF EXISTS `django_flatpage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_flatpage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(100) NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` longtext NOT NULL,
  `enable_comments` tinyint(1) NOT NULL,
  `template_name` varchar(70) NOT NULL,
  `registration_required` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_flatpage_a4b49ab` (`url`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_flatpage`
--

LOCK TABLES `django_flatpage` WRITE;
/*!40000 ALTER TABLE `django_flatpage` DISABLE KEYS */;
INSERT INTO `django_flatpage` VALUES (1,'/about/','О компании','Информация о компании',0,'flatpages/about.html',0),(2,'/contacts/','Все контакты','Контактная информация',0,'flatpages/contacts.html',0),(3,'/map/','Схема проезда','Здесь будет схема проезда',0,'flatpages/map.html',0),(4,'/rules/','Правила использования калькулятора','Список правил использования\r\n---------------------------------------------------------\r\n1. Правило 1\r\n2. Правило 2\r\n3. Правило 3\r\n\r\n',0,'flatpages/rules.html',0);
/*!40000 ALTER TABLE `django_flatpage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_flatpage_sites`
--

DROP TABLE IF EXISTS `django_flatpage_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_flatpage_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flatpage_id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `flatpage_id` (`flatpage_id`,`site_id`),
  KEY `django_flatpage_sites_dedefef8` (`flatpage_id`),
  KEY `django_flatpage_sites_6223029` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_flatpage_sites`
--

LOCK TABLES `django_flatpage_sites` WRITE;
/*!40000 ALTER TABLE `django_flatpage_sites` DISABLE KEYS */;
INSERT INTO `django_flatpage_sites` VALUES (17,1,1),(14,2,1),(15,3,1),(20,4,1);
/*!40000 ALTER TABLE `django_flatpage_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_c25c2c28` (`expire_date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('735a54d37f563b6f2f67a31398ecdfd3','YzhiNTg1NTRlYmY1M2VlOGViZTg1MmE1M2I4NjdkYjdkMzJjYzUxZjqAAn1xAVUKdGVzdGNvb2tp\nZVUGd29ya2VkcQJzLg==\n','2012-07-17 15:39:13'),('d09885c407c4855c4d946402e97889c2','NmU5ZmYyYTQ4N2NkMTAyNmJmOGM3ZGUzMDc4Mzg3NDU5M2I1MmUyNTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n','2012-07-18 16:22:31'),('e788a31ac64c4c16f05e2739294a566f','NmU5ZmYyYTQ4N2NkMTAyNmJmOGM3ZGUzMDc4Mzg3NDU5M2I1MmUyNTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZHECVSlkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZHED\nVQ1fYXV0aF91c2VyX2lkcQSKAQF1Lg==\n','2012-07-19 12:30:41'),('ea2007af3c6f20329ee842a89e5cc676','NGJlNzk2ZTMyNDIzMDU3M2VhYTRmYmIyYzgxNjFhMTA2M2VkMDFkMTqAAn1xAS4=\n','2012-07-14 13:16:21'),('39092d86021323d4d64c5c0769f81854','NmM2NGFhOWM1MzNjZTc4OWE3NDEzZTEzOWM2MTdhMWIyNjBiMTQ0NTqAAn1xAShVEl9hdXRoX3Vz\nZXJfYmFja2VuZFUpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmRVDV9h\ndXRoX3VzZXJfaWSKAQF1Lg==\n','2012-07-18 12:03:54'),('9d11cc8198c09a8233344014c8724e47','NGJlNzk2ZTMyNDIzMDU3M2VhYTRmYmIyYzgxNjFhMTA2M2VkMDFkMTqAAn1xAS4=\n','2012-07-14 13:09:52');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_site`
--

DROP TABLE IF EXISTS `django_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_site`
--

LOCK TABLES `django_site` WRITE;
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;
INSERT INTO `django_site` VALUES (1,'oasis.ru','oasis.ru');
/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_page`
--

DROP TABLE IF EXISTS `page_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lft` int(10) unsigned NOT NULL,
  `rght` int(10) unsigned NOT NULL,
  `tree_id` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  `active` tinyint(1) NOT NULL,
  `title` varchar(200) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `in_navigation` tinyint(1) NOT NULL,
  `override_url` varchar(300) NOT NULL,
  `redirect_to` varchar(300) NOT NULL,
  `_cached_url` varchar(300) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `page_page_42b06ff6` (`lft`),
  KEY `page_page_91543e5a` (`rght`),
  KEY `page_page_efd07f28` (`tree_id`),
  KEY `page_page_2a8f42e8` (`level`),
  KEY `page_page_a951d5d6` (`slug`),
  KEY `page_page_63f17a16` (`parent_id`),
  KEY `page_page_f009b407` (`_cached_url`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_page`
--

LOCK TABLES `page_page` WRITE;
/*!40000 ALTER TABLE `page_page` DISABLE KEYS */;
/*!40000 ALTER TABLE `page_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_advice`
--

DROP TABLE IF EXISTS `service_advice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_advice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `body` longtext NOT NULL,
  `publication_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_advice`
--

LOCK TABLES `service_advice` WRITE;
/*!40000 ALTER TABLE `service_advice` DISABLE KEYS */;
INSERT INTO `service_advice` VALUES (1,'Покраска','Используйте краски фирмы %noname%','2012-06-30 12:33:12');
/*!40000 ALTER TABLE `service_advice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_article`
--

DROP TABLE IF EXISTS `service_article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `body` longtext NOT NULL,
  `publication_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_article`
--

LOCK TABLES `service_article` WRITE;
/*!40000 ALTER TABLE `service_article` DISABLE KEYS */;
INSERT INTO `service_article` VALUES (1,'Как правильно ложить пол','Собственно статья о том, как это делать.','2012-06-30 12:33:12');
/*!40000 ALTER TABLE `service_article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_bonus`
--

DROP TABLE IF EXISTS `service_bonus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_bonus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(40) NOT NULL,
  `description` longtext NOT NULL,
  `size` int(11) NOT NULL,
  `metric` varchar(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_bonus`
--

LOCK TABLES `service_bonus` WRITE;
/*!40000 ALTER TABLE `service_bonus` DISABLE KEYS */;
INSERT INTO `service_bonus` VALUES (1,'Скидка на все услуги в июне!','Скидка на все услуги, оказываемые в июне 2012 года.',10,'%'),(2,'Скидка на сантехнику!','Скидка на установку сантехники!',20,'%'),(3,'Скидка на поклейку обоев!','Только с 27 июня по 27 июля дейсвует скидка на поклейку обоев!',10,'руб');
/*!40000 ALTER TABLE `service_bonus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_bonus_service`
--

DROP TABLE IF EXISTS `service_bonus_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_bonus_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bonus_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bonus_id` (`bonus_id`,`service_id`),
  KEY `service_bonus_service_2ec987c0` (`bonus_id`),
  KEY `service_bonus_service_90e28c3e` (`service_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_bonus_service`
--

LOCK TABLES `service_bonus_service` WRITE;
/*!40000 ALTER TABLE `service_bonus_service` DISABLE KEYS */;
INSERT INTO `service_bonus_service` VALUES (1,1,1),(2,1,2),(3,1,3),(4,1,4),(5,1,5),(6,1,6),(7,1,7),(8,1,8),(9,2,4),(11,3,7);
/*!40000 ALTER TABLE `service_bonus_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_bonuses_service`
--

DROP TABLE IF EXISTS `service_bonuses_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_bonuses_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bonuses_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bonuses_id` (`bonuses_id`,`service_id`),
  KEY `service_bonuses_service_2227fb82` (`bonuses_id`),
  KEY `service_bonuses_service_90e28c3e` (`service_id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_bonuses_service`
--

LOCK TABLES `service_bonuses_service` WRITE;
/*!40000 ALTER TABLE `service_bonuses_service` DISABLE KEYS */;
INSERT INTO `service_bonuses_service` VALUES (18,1,8),(17,1,7),(16,1,6),(15,1,5),(14,1,4),(13,1,3),(12,1,2),(11,1,1),(10,2,4);
/*!40000 ALTER TABLE `service_bonuses_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_comment`
--

DROP TABLE IF EXISTS `service_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `body` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `service_comment_fbfc09f1` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_comment`
--

LOCK TABLES `service_comment` WRITE;
/*!40000 ALTER TABLE `service_comment` DISABLE KEYS */;
INSERT INTO `service_comment` VALUES (1,1,'Заказывал пол! Отличная работа!'),(2,11,'Имела счастье работать с этой компанией, остались только положительные эмоции, спасибо! )'),(3,1,'Комментарий');
/*!40000 ALTER TABLE `service_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_gallery`
--

DROP TABLE IF EXISTS `service_gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(40) NOT NULL,
  `description` longtext,
  `finish` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_gallery`
--

LOCK TABLES `service_gallery` WRITE;
/*!40000 ALTER TABLE `service_gallery` DISABLE KEYS */;
INSERT INTO `service_gallery` VALUES (1,'Галлерея 1','',1),(2,'Галлерея 2','',1),(3,'Галлерея 3','',0),(4,'Галлерея 4','',0);
/*!40000 ALTER TABLE `service_gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_metric_service`
--

DROP TABLE IF EXISTS `service_metric_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_metric_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `metric_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `metric_id` (`metric_id`,`service_id`),
  KEY `service_metric_service_85fa1a12` (`metric_id`),
  KEY `service_metric_service_90e28c3e` (`service_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_metric_service`
--

LOCK TABLES `service_metric_service` WRITE;
/*!40000 ALTER TABLE `service_metric_service` DISABLE KEYS */;
INSERT INTO `service_metric_service` VALUES (1,1,8),(2,1,9),(3,1,10),(4,2,9),(5,2,5);
/*!40000 ALTER TABLE `service_metric_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_order`
--

DROP TABLE IF EXISTS `service_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `summary` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `performed` tinyint(1) NOT NULL,
  `remark` longtext,
  PRIMARY KEY (`id`),
  KEY `service_order_8337030b` (`order_id`),
  KEY `service_order_fbfc09f1` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_order`
--

LOCK TABLES `service_order` WRITE;
/*!40000 ALTER TABLE `service_order` DISABLE KEYS */;
INSERT INTO `service_order` VALUES (9,4,7200,20,11,'2012-07-03 11:36:55',0,''),(8,3,3600,10,11,'2012-07-03 11:36:55',0,''),(10,4,360,1,1,'2012-07-04 11:57:35',0,''),(11,3,360,1,1,'2012-07-04 12:00:04',0,''),(12,4,360,1,1,'2012-07-04 12:00:04',0,''),(13,4,720,2,1,'2012-07-04 12:03:30',0,'');
/*!40000 ALTER TABLE `service_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_photo`
--

DROP TABLE IF EXISTS `service_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_gallery_id` int(11) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `service_photo_61084593` (`parent_gallery_id`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_photo`
--

LOCK TABLES `service_photo` WRITE;
/*!40000 ALTER TABLE `service_photo` DISABLE KEYS */;
INSERT INTO `service_photo` VALUES (45,3,'img/11_2.jpg',''),(44,3,'img/10_3.jpg',''),(43,3,'img/9_2.JPG',''),(42,3,'img/8_2.jpeg',''),(41,2,'img/6_2.jpeg',''),(40,2,'img/5_2.jpeg',''),(39,2,'img/4_2.jpeg',''),(38,1,'img/7_2.jpeg',''),(37,1,'img/3_2.jpeg',''),(36,1,'img/1_2.jpeg','');
/*!40000 ALTER TABLE `service_photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_sertificate`
--

DROP TABLE IF EXISTS `service_sertificate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_sertificate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sertificate` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `size` int(11) NOT NULL,
  `metric` varchar(4) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `service_sertificate_fbfc09f1` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_sertificate`
--

LOCK TABLES `service_sertificate` WRITE;
/*!40000 ALTER TABLE `service_sertificate` DISABLE KEYS */;
INSERT INTO `service_sertificate` VALUES (3,'654321',1,10,'%',0),(2,'123456',1,1000,'руб',1);
/*!40000 ALTER TABLE `service_sertificate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_service`
--

DROP TABLE IF EXISTS `service_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(40) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `description` longtext,
  `miniature` varchar(100) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `metric` varchar(40) NOT NULL,
  `lft` int(10) unsigned NOT NULL,
  `rght` int(10) unsigned NOT NULL,
  `tree_id` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `service_service_63f17a16` (`parent_id`),
  KEY `service_service_42b06ff6` (`lft`),
  KEY `service_service_91543e5a` (`rght`),
  KEY `service_service_efd07f28` (`tree_id`),
  KEY `service_service_2a8f42e8` (`level`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_service`
--

LOCK TABLES `service_service` WRITE;
/*!40000 ALTER TABLE `service_service` DISABLE KEYS */;
INSERT INTO `service_service` VALUES (1,'Ремонт квартир',NULL,'','',NULL,'м 2',1,14,1,0),(2,'Отделка офисов',NULL,'','',NULL,'м 2',1,8,2,0),(3,'Отделочные работы',1,'','',400,'м 2',2,3,1,1),(4,'Сантехника',1,'','',500,'шт.',4,11,1,1),(5,'Малярные работы',1,'','',300,'м 2',12,13,1,1),(6,'Укладка плитки',2,'','',1000,'м 2',2,3,2,1),(7,'Поклейка обоев',2,'','',15,'м 2',4,5,2,1),(8,'Пол',2,'','',73,'м 2',6,7,2,1),(9,'Установка ванн',4,'','',NULL,'шт.',5,6,1,2),(10,'Установка унитазов',4,'','',NULL,'шт.',7,8,1,2),(11,'Прокладка труб',4,'','',NULL,'м.',9,10,1,2);
/*!40000 ALTER TABLE `service_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_userprofile`
--

DROP TABLE IF EXISTS `service_userprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_userprofile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `confirm_code` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_userprofile`
--

LOCK TABLES `service_userprofile` WRITE;
/*!40000 ALTER TABLE `service_userprofile` DISABLE KEYS */;
INSERT INTO `service_userprofile` VALUES (9,11,'84220185e0466c9c919eaa73a24c843b4045bb24ac8ed1ba0f007fa3');
/*!40000 ALTER TABLE `service_userprofile` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-07-05 13:10:26
